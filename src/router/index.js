import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/home.vue";
import todolist from "@/components/theToDoList.vue";
import todoform from "@/components/theTodoForm.vue";

Vue.use(VueRouter);
const routes = [
  {
    path: "/",
    name: "home",
    component: Home,
  },
  {
    path: "/login",
    name: "login",
    component: () =>
      import(/* webpackChunkName: "login" */ "../views/login.vue"),
  },
  {
    path: "/dashboard",
    redirect: "/dashboard/list",
    name: "dashboard",
    component: () =>
      import(
        /* webpackChunkName: "dashboard" */ "../views/dashboard/dashboard.vue"
      ),
    children: [
      {
        path: "list",
        name: "dahsboard",
        component: todolist,
      },
      {
        path: "add",
        name: "newTodo",
        component: todoform,
      },
      {
        path: "edit/:id",
        name: "editTodo",
        component: todoform,
      },
      {
        path: "",
        redirect: "list",
      },
    ],
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
