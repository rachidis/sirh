export default class Todo {
  constructor(text = "", desc = "", isDone = false) {
    this.id = null;
    this.date = new Date();
    this.date = Todo.randomDate(new Date(2019, 0, 1), new Date());
    this.text = text;
    this.desc = desc;
    this.isDone = isDone;
  }

  static randomDate(start, end) {
    return new Date(
      start.getTime() + Math.random() * (end.getTime() - start.getTime())
    );
  }
}

export const TodoStates = {
  waiting: false,
  done: true,
  all: "all",
};
