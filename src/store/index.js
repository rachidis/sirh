import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);
import { TodoStates } from "@/models/Todo.js";

export default new Vuex.Store({
  state: {
    isLoading: false,
    isLogged: false,
    user: {
      name: "Rachid",
    },
    todos: [
      {
        id: 1548224630296,
        date: "2019-05-05T13:48:33.178Z",
        text: "application",
        desc:
          "developper une application en utilisant vuejs et material design",
        isDone: false,
      },
      {
        id: 1548224650396,
        date: "2019-02-10T21:08:07.829Z",
        text: "fuga atque? sit ",
        desc: "undefined atque? Ea, adipisicing ",
        isDone: false,
      },
      {
        id: 1582777156221,
        date: "2019-07-11T18:34:46.998Z",
        text: "sit ",
        desc: "optio optio sit Lorem ab ipsam eum. ipsum Ea, ",
        isDone: true,
      },
      {
        id: 1582777126221,
        text: "une tache simple",
        date: "2019-02-10T21:08:07.829Z",
        desc: "il faut faire ça",
        isDone: true,
      },
      {
        id: 1569539739378,
        date: "2020-06-04T06:19:16.979Z",
        text: "ab ",
        desc: "Ea, ipsum ipsum sit ",
        isDone: false,
      },
      {
        id: 1579350744634,
        date: "2019-09-26T22:17:54.465Z",
        text: "ab Facere dolor ",
        desc: "quos amet Lorem ",
        isDone: true,
      },
      {
        id: 1561889145009,
        date: "2019-06-03T17:48:45.547Z",
        text: "atque? elit. ",
        desc: "assumenda sit ",
        isDone: false,
      },
      {
        id: 1570028929963,
        date: "2019-05-05T13:48:33.178Z",
        text: "qsdqd",
        desc: "ipsam assumenda veritatis, amet assumenda amet Lorem  itaque ",
        isDone: false,
      },
      {
        id: 1567391858169,
        date: "2019-06-05T12:48:13.620Z",
        text: "optio ",
        desc: "Facere Facere quos elit. amet sit vitae ",
        isDone: false,
      },
      {
        id: 1582202318798,
        date: "2020-04-18T20:58:33.616Z",
        text: "fuga ",
        desc: "dolor ipsam fuga optio  ",
        isDone: false,
      },
      {
        id: 1555580554110,
        date: "2020-06-17T07:49:53.339Z",
        text: "fuga fugit quos ",
        desc: "atque? sit atque? ",
        isDone: false,
      },
      {
        id: 1585307936249,
        date: "2020-02-22T11:56:12.856Z",
        text: "amet itaque vitae dolor ",
        desc: "quia fugit consectetur amet assumenda fugit fugit ",
        isDone: false,
      },
      {
        id: 1582787260528,
        date: "2019-07-02T12:38:05.433Z",
        text: "ipsum quia ",
        desc: "quas vitae ab ipsum ",
        isDone: true,
      },
      {
        id: 1570120673225,
        date: "2019-09-12T22:19:39.915Z",
        text: "elit. Lorem ",
        desc: "fuga quia eum. quia ipsum assumenda ab optio elit. fugit ",
        isDone: true,
      },
      {
        id: 1566613720616,
        date: "2019-11-16T21:37:37.203Z",
        text: "voluptatum ",
        desc: "ipsum magnam eum. Facere totam totam quas quas assumenda ",
        isDone: false,
      },
      {
        id: 1585321496374,
        date: "2020-06-06T09:45:31.168Z",
        text: "amet assumenda ",
        desc: "elit. elit. assumenda atque? quas ",
        isDone: true,
      },
      {
        id: 1581451845501,
        date: "2019-11-04T11:26:48.216Z",
        text: " qsdqd qsd",
        desc: "quos blanditiis ",
        isDone: true,
      },
      {
        id: 1591154586910,
        date: "2019-04-11T04:07:31.099Z",
        text: "os blandi",
        desc: "Facere elit. eum. quos vitae ",
        isDone: false,
      },
      {
        id: 1592361172976,
        date: "2019-12-24T22:50:26.953Z",
        text: "itaque ",
        desc: "dolor voluptatum amet atque? elit. ",
        isDone: true,
      },
      {
        id: 1581222915412,
        date: "2020-07-02T04:46:00.068Z",
        text: "lor voluptatum op",
        desc: " voluptatum ipsam voluptatum quia ",
        isDone: true,
      },
      {
        id: 1552713252498,
        date: "2019-05-16T16:04:38.619Z",
        text: "itaque quas ",
        desc: "Lorem dolor voluptatum optio vitae ",
        isDone: true,
      },
      {
        id: 1556564419114,
        date: "2020-06-16T18:30:57.906Z",
        text: "Lorem vitae ",
        desc: "vitae ipsum dolor ",
        isDone: false,
      },
    ],
  },
  getters: {
    todos: (state) =>
      state.todos.sort((a, b) => new Date(b.date) - new Date(a.date)),
    doneTodos: (state) =>
      state.todos
        .filter((todo) => todo.isDone)
        .sort((a, b) => new Date(b.date) - new Date(a.date)),
    waitingTodos: (state) =>
      state.todos
        .filter((todo) => !todo.isDone)
        .sort((a, b) => new Date(b.date) - new Date(a.date)),
    getById: (state) => (id) => state.todos.find((todo) => todo.id == id),
    getByPageNumber: (state, getters) => (
      page_size,
      page_number,
      state = TodoStates.all
    ) => {
      let todos = getters.todos;
      if (state != TodoStates.all) {
        todos = todos.filter((todo) => todo.isDone == state);
      }
      todos = todos.slice(
        (page_number - 1) * page_size,
        page_number * page_size
      );
      return todos;
    },
    loadingState: (state) => state.isLoading,
    logingState: (state) => state.isLogged,
  },
  mutations: {
    showLoader(state) {
      state.isLoading = true;
    },
    hideLoader(state) {
      state.isLoading = false;
    },
    changeLoginState(state, loginState) {
      state.isLogged = loginState;
    },
    changeState(state, params) {
      const { id , status } = params;
      let index =this.getters.todos.map(todo=>todo.id).indexOf(id)
      this.getters.todos[index].isDone = status;
    },
    deleteTodo(state, id) {
      let index =this.getters.todos.map(todo=>todo.id).indexOf(id)
      this.getters.todos.splice(index, 1);
    },
    addTodo(state, todo) {
      todo = JSON.parse(JSON.stringify(todo));
      state.todos.push(todo);
    },
    editTodo(state, todo) {
      let toBeModified = state.todos.find((arrTodo) => arrTodo.id == todo.id);
      if (toBeModified) {
        let index = state.todos.indexOf(toBeModified);
        state.todos[index] = todo;
      }
    },
  },
  actions: {
    addTodo({ commit }, todo) {
      return new Promise((res) => {
        setTimeout(() => {
          todo.id = new Date().getTime();
          commit("addTodo", todo);
          res(todo.id);
        }, 1000);
      });
    },
    editTodo({ commit }, todo) {
      return new Promise((res) => {
        setTimeout(() => {
          commit("editTodo", todo);
          res(todo.id);
        }, 1000);
      });
    },
  },
  modules: {},
});
