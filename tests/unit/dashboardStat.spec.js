import { shallowMount } from "@vue/test-utils";
import dashboardStats from "@/components/theDashboardStats.vue";
import Vuetify from "vuetify";
import Vue from "vue";
Vue.use(Vuetify);

describe("login.vue", () => {
  let wrapper = "";
  beforeEach(() => {
    wrapper = shallowMount(dashboardStats);
  });
  it("expect donetasks to be number", () => {
    expect(wrapper.vm.doneTasks).toBeTruthy();
  });
  it("expect donetasks to be number", () => {
    expect(wrapper.vm.waitingTasks).toBeTruthy();
  });
  it("expect donetasks to be number", () => {
    expect(wrapper.vm.allTasks).toBeTruthy();
  });
  
  it("should call filter function when card is click", () => {
    let filterFunction = jest.fn();
    wrapper.setMethods({
      filter:filterFunction
    })
    wrapper.find('.card').trigger('click');
    expect(filterFunction).toHaveBeenCalled();
  });
});
