import { shallowMount } from "@vue/test-utils";
import theLoginForm from "@/components/theLoginForm.vue";
import Vuetify from "vuetify";
import Vue from "vue";
Vue.use(Vuetify);

describe("login.vue", () => {
  let wrapper = "";
  beforeEach(() => {
    wrapper = shallowMount(theLoginForm);
  });
  it("has email input", () => {
    expect(wrapper.find("#logininput").exists()).toBeTruthy();
  });
  it("has pw input", () => {
    expect(wrapper.find("#passwordinput").exists()).toBeTruthy();
  });
});
